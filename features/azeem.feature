Feature: Azeem application testing

Scenario: Azeem home page testing

Given users on azeem web page
When user click on about
Then user click on photographer
And user verify text photos

Scenario: testing stories page
When users click on stories
And users click on tbilisi
Then user verify text Tbilisi, Georgia

Scenario: testing About page
When users click on 'about'
And users click on 'contact me'
And user fill 'name'
And user fill 'Email'
And user fill 'Message'
And user click 'Send'
Then user see error message
 