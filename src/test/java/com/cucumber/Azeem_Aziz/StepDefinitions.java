package com.cucumber.Azeem_Aziz;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitions {
	
	WebDriver Driver;

	@Given("^users on azeem web page$")
	public void users_on_azeem_web_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "target/res/chromedriver.exe");
		Driver = new ChromeDriver();
		Driver.get("https://azeem.me/");
	    
	}

	@When("^user click on about$")
	public void user_click_on_about() throws Throwable {
		Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Driver.findElement(By.xpath("//*[@id='menu-about']")).click();
	    
	}

	@Then("^user click on photographer$")
	public void user_click_on_photographer() throws Throwable {
		Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Driver.findElement(By.xpath("//*[text()='photographer']")).click();
	    
	}

	@And("^user verify text photos$")
	public void user_verify_text_photos() throws Throwable {
		Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Driver.getPageSource().contains("Photos");
	    
	}
	
	@When("^users click on stories$")
	public void users_click_on_stories() throws Throwable {
		Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Driver.findElement(By.xpath("//*[text()='stories']")).click();
	    
	}

	@And("^users click on tbilisi$")
	public void users_click_on_tbilisi() throws Throwable {
		Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Driver.findElement(By.xpath("//a[@href='https://azeem.me/stories/tbilisi-georgia/']")).click();
	    
	}

	@Then("^user verify text Tbilisi, Georgia$")
	public void user_verify_text_Tbilisi_Georgia() throws Throwable {
		Driver.getPageSource().contains("Tbilisi, Georgia");
	}
	
	@When("^users click on 'about'$")
	public void users_click_on_about() throws Throwable {
		Driver.findElement(By.xpath("//*[@id='menu-about']")).click();
	    
	}

	@When("^users click on 'contact me'$")
	public void users_click_on_contact_me() throws Throwable {
		Driver.findElement(By.xpath("//a[@href='/contact/' and text()='contact me']")).click();
	    
	}

	@When("^user fill 'name'$")
	public void user_fill_name() throws Throwable {
		Driver.findElement(By.xpath("//*[@name='your-name']")).sendKeys("Abcdefg");
	    
	}

	@When("^user fill 'Email'$")
	public void user_fill_Email() throws Throwable {
		Driver.findElement(By.xpath("//*[@name='your-email']")).sendKeys("Abcdefg");
	    
	}

	@When("^user fill 'Message'$")
	public void user_fill_Message() throws Throwable {
		Driver.findElement(By.xpath("//*[@name='your-message']")).sendKeys("Abcdefg");
	    
	}

	@When("^user click 'Send'$")
	public void user_click_Send() throws Throwable {
		Driver.findElement(By.xpath("//*[@value='Send']")).click();
	    
	}

	@Then("^user see error message$")
	public void user_see_error_message() throws Throwable {
		Driver.getPageSource().contains("Validation errors occurred. Please confirm the fields and submit it again.");
	    
	}

}
